using RenderHeads.Media.AVProMovieCapture;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace UnityEngine.MovieCapture
{
    public class RecordItem : MonoBehaviour
    {
        public Text videoName;
        public Text videoSize;
        public Text videoLength;
        public Text lastModifyTime;
        public Button view;
        public VideoControl videoControl;
        private VideoInfo videoInfo;

        // Start is called before the first frame update
        void Start()
        {
            view.onClick.AddListener(() => {
                videoControl.VideoControlInit(videoInfo.videoPath);
            });
        }

        /// <summary>
        /// ��ʼ����¼��
        /// </summary>
        /// <param name="saveInfo"></param>
        public void InitRecordItem(VideoInfo videoInfo)
        {
            this.videoInfo = videoInfo;
            videoName.text = videoInfo.videoName;
            videoSize.text = (videoInfo.videoSize / (1024f * 1024f)).ToString("F1") + "MB";
            videoLength.text = (videoInfo.videoLength / 60).ToString("00") + ":" + (videoInfo.videoLength % 60).ToString("00");
            lastModifyTime.text = videoInfo.lastModifyTime;
        }
    }
}

