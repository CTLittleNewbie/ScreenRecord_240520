using RenderHeads.Media.AVProMovieCapture;
using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using UnityEngine;
using UnityEngine.UI;

namespace UnityEngine.MovieCapture
{
    [Serializable]
    public class VideoInfo
    {
        public string videoName;
        public long videoSize;
        public uint videoLength;
        public string videoPath;
        public string lastModifyTime;
    }

    public enum OutputPath
    {
        RelativeToProject,
        RelativeToAssets,
        RelativeToStreamingAssets,
        RelativeToPeristentData,
        Absolute,
        RelativeToDesktop,
    }

    public class ScreenRecord : MonoBehaviour
    {
        private CaptureBase _movieCapture;

        public Button startRecord;
        public Button stopRecord;
        public Button lastCapture;
        public Text fileSize;
        public Text videoLength;
        public RecordList recordList;
        private List<VideoInfo> videoList = new List<VideoInfo>();

        private long _lastFileSize;
        private uint _lastEncodedMinutes;
        private uint _lastEncodedSeconds;
        private uint _lastEncodedFrame;

        private StringBuilder _fileSize = new StringBuilder();
        private StringBuilder _videoLength = new StringBuilder();

        private string videoOutFolder;
        public const OutputPath defaultOutputFolderType = OutputPath.RelativeToStreamingAssets;
        public const string defaultOutputFolderPath = "Captures";

        public OutputPath outputFolderType = defaultOutputFolderType;
        public string outputFolderPath = defaultOutputFolderPath;

        public CaptureBase.DownScale downScale = CaptureBase.DownScale.Original;
        public float frame = 60;
        public string prefix = "ScreenCapture";

        private void Awake()
        {
            _movieCapture = GetComponent<CaptureBase>();

            startRecord.onClick.AddListener(() =>
            {
                StartCapture();
            });

            stopRecord.onClick.AddListener(() =>
            {
                StopCapture();

                RecordSave();
            });

            lastCapture.onClick.AddListener(() =>
            {

                recordList.InitRecordList(videoList);
            });

            stopRecord.interactable = false;
        }

        // Start is called before the first frame update
        void Start()
        {
            SetPrefix();
            SetOutFolderPath();
            SetDownScale();
            SetFrameRate();
        }

        private void Update()
        {
            if (_movieCapture.IsCapturing())
            {
                FileSize();

                VideoLength();
            }
        }

        private void SetPrefix()
        {
            _movieCapture.FilenamePrefix = prefix;
        }

        /// <summary>
        /// 设置视频输出路径
        /// </summary>
        private void SetOutFolderPath()
        {
            videoOutFolder = GetFolder(outputFolderType, outputFolderPath);

            if (!Directory.Exists(videoOutFolder))
                Directory.CreateDirectory(videoOutFolder);
            _movieCapture.OutputFolderPath = videoOutFolder;
        }

        /// <summary>
        /// Returns the folder path based on the given output path type and path.
        /// </summary>
        /// <param name="outputPathType">The type of output path.</param>
        /// <param name="path">The relative path to the folder.</param>
        /// <returns>The full path to the folder.</returns>
        private string GetFolder(OutputPath outputPathType, string path)
        {
            string folder = string.Empty;

            switch (outputPathType)
            {
                case OutputPath.RelativeToProject:
                    folder = Path.GetFullPath(Path.Combine(Application.dataPath, ".."));
                    break;
                case OutputPath.RelativeToAssets:
                    folder = Path.GetFullPath(Application.dataPath);
                    break;
                case OutputPath.RelativeToStreamingAssets:
                    folder = Path.GetFullPath(Application.streamingAssetsPath);
                    break;
                case OutputPath.RelativeToPeristentData:
                    folder = Path.GetFullPath(Application.persistentDataPath);
                    break;
                case OutputPath.Absolute:
                    break;
                case OutputPath.RelativeToDesktop:
                    folder = Environment.GetFolderPath(Environment.SpecialFolder.DesktopDirectory);
                    break;
                default:
                    break;
            }

            return Path.Combine(folder, path);
        }

        /// <summary>
        /// 设置视频画质
        /// </summary>
        private void SetDownScale()
        {
            _movieCapture.ResolutionDownScale = downScale;
        }

        /// <summary>
        /// 设置帧率
        /// </summary>
        private void SetFrameRate()
        {
            _movieCapture.FrameRate = frame;

        }

        /// <summary>
        /// 开始录制
        /// </summary>
        private void StartCapture()
        {
            _lastFileSize = 0;
            _lastEncodedMinutes = _lastEncodedSeconds = _lastEncodedFrame = 0;
            if (_movieCapture != null)
            {
                _movieCapture.StartCapture();
                startRecord.interactable = false;
                stopRecord.interactable = true;
            }
        }

        /// <summary>
        /// 停止录制
        /// </summary>
        private void StopCapture()
        {
            if (_movieCapture != null)
            {
                if (_movieCapture.IsCapturing())
                {
                    _movieCapture.StopCapture();
                    startRecord.interactable = true;
                    stopRecord.interactable = false;
                }
            }
        }

        /// <summary>
        /// 文件大小设置
        /// </summary>
        /// <param name="lastFileSize"></param>
        private void FileSize()
        {
            _lastFileSize = _movieCapture.GetCaptureFileSize();

            _fileSize.Clear();
            _fileSize.Append((_lastFileSize / (1024f * 1024f)).ToString("F1"));
            _fileSize.Append("MB");

            fileSize.text = _fileSize.ToString();
        }

        /// <summary>
        /// 视频长度设置
        /// </summary>
        private void VideoLength()
        {
            if (!_movieCapture.IsRealTime)
            {
                _lastEncodedSeconds = (uint)Mathf.FloorToInt(_movieCapture.CaptureStats.NumEncodedFrames / _movieCapture.FrameRate);
            }
            else
            {
                _lastEncodedSeconds = _movieCapture.CaptureStats.TotalEncodedSeconds;
            }

            _lastEncodedMinutes = _lastEncodedSeconds / 60;
            _lastEncodedSeconds = _lastEncodedSeconds % 60;
            _lastEncodedFrame = _movieCapture.CaptureStats.NumEncodedFrames % (uint)_movieCapture.FrameRate;

            _videoLength.Clear();

            _videoLength.Append(_lastEncodedMinutes.ToString("00"));
            _videoLength.Append(":");
            _videoLength.Append(_lastEncodedSeconds.ToString("00"));
            _videoLength.Append(".");
            _videoLength.Append(_lastEncodedFrame.ToString("000"));

            videoLength.text = _videoLength.ToString();
        }

        /// <summary>
        /// 视频保存
        /// </summary>
        /// <param name="saveInfo"></param>
        private void RecordSave()
        {
            VideoInfo videoInfo = new VideoInfo();

            videoInfo.videoName = Path.GetFileNameWithoutExtension(CaptureBase.LastFileSaved);
            videoInfo.videoSize = _lastFileSize;
            videoInfo.videoLength = _lastEncodedSeconds;
            videoInfo.videoPath = CaptureBase.LastFileSaved;
            videoInfo.lastModifyTime = DateTime.Now.ToString("G");

            videoList.Add(videoInfo);
        }
    }
}

