using System.Collections.Generic;
using UnityEngine;

namespace UnityEngine.MovieCapture
{
    public class RecordList : MonoBehaviour
    {
        public RecordItem recordItemPre;
        private List<RecordItem> records = new List<RecordItem>();

        /// <summary>
        /// ��ʼ����¼��
        /// </summary>
        /// <param name="saveInfos"></param>
        public void InitRecordList(List<VideoInfo> videoList)
        {
            gameObject.SetActive(true);

            for (int i = 0; i < records.Count; i++)
            {
                Destroy(records[i].gameObject);
            }

            records.Clear();

            for (int i = 0; i < videoList.Count; i++)
            {
                RecordItem recordItem = Instantiate(recordItemPre, recordItemPre.transform.parent);
                recordItem.InitRecordItem(videoList[i]);
                records.Add(recordItem);
                recordItem.gameObject.SetActive(true);
            }
        }
    }
}


