using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace UnityEngine.MovieCapture
{
    [System.Serializable]
    public class SaveInfo
    {
        public string operationName;
        public string operationPeople;
        public string recordTime;
        public string recordPath;
    }
    public class RecordSave : MonoBehaviour
    {
        public InputField operationName;
        public InputField operationPeople;
        public InputField recordTime;

        public Button save;
        public Button cancle;

        public Action<SaveInfo> saveEvent;

        private void OnEnable()
        {
            operationName.text = "";
            operationPeople.text = "";
            recordTime.text = DateTime.Now.ToString("G");
        }

        // Start is called before the first frame update
        void Start()
        {
            recordTime.text = DateTime.Now.ToString("G");

            save.onClick.AddListener(() =>
            {
                SaveInfo saveInfo = new SaveInfo();
                saveInfo.operationName = operationName.text;
                saveInfo.operationPeople = operationPeople.text;
                saveInfo.recordTime = recordTime.text;
                saveEvent?.Invoke(saveInfo);

                gameObject.SetActive(false);
            });

            cancle.onClick.AddListener(() =>
            {
                gameObject.SetActive(false);
            });
        }
    }
}

