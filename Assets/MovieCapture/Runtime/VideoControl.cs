using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;
using UnityEngine.Assertions.Must;
using UnityEngine.UI;
using UnityEngine.Video;

namespace UnityEngine.MovieCapture
{
    public class VideoControl : MonoBehaviour
    {
        public Text title;
        public VideoPlayer player;
        public Slider videoSlider;
        public Button close;
        public Text videotime;
        public Text videoLength;

        string videoPath;

        private void Update()
        {
            if (player.isPlaying)
            {
                SetCurPlayTime();
                SetPlaySchedule();
            }
        }

        private void Awake()
        {
            player.prepareCompleted += OnPrepareCompleted;
        }

        private void OnDestroy()
        {
            player.prepareCompleted -= OnPrepareCompleted;
        }

        public void VideoControlInit(string videoPath)
        {
            this.videoPath = videoPath;

            gameObject.SetActive(true);
            SetVideoPlayPath(videoPath);
            PlayVideo();
        }

        private void OnPrepareCompleted(VideoPlayer videoSource)
        {
            SetVideoName();
            SetVideoLength();
        }

        private void SetVideoName()
        {
            title.text = Path.GetFileNameWithoutExtension(videoPath);
        }

        /// <summary>
        /// 设置播放器路径
        /// </summary>
        /// <param name="path"></param>
        public void SetVideoPlayPath(string path)
        {
            player.url = path;
        }

        /// <summary>
        /// 当前视频播放时间
        /// </summary>
        private void SetCurPlayTime()
        {
            videotime.text = TimeProcess(player.time);
        }

        /// <summary>
        /// 设置视频时长
        /// </summary>
        /// <param name="length"></param>
        private void SetVideoLength()
        {
            videoLength.text = TimeProcess(player.length);
        }

        /// <summary>
        /// 设置播放进度
        /// </summary>
        private void SetPlaySchedule()
        {
            videoSlider.value = (float)player.frame / player.frameCount;
        }

        /// <summary>
        /// 时间处理
        /// </summary>
        /// <param name="time"></param>
        /// <returns></returns>
        private string TimeProcess(double time)
        {
            int minutes = (int)time / 60;
            int seconds = (int)time % 60;
            return minutes.ToString("00") + ":" + seconds.ToString("00");
        }

        /// <summary>
        /// 视频播放
        /// </summary>
        public void PlayVideo()
        {
            player.Play();
        }
    }
}

