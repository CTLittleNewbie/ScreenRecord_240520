using RenderHeads.Media.AVProMovieCapture;
using RenderHeads.Media.AVProMovieCapture.Editor;
using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;
using UnityEngine.MovieCapture;

namespace UnityEditor.MovieCapture
{
    /// <summary>
    /// Custom editor for the ScreenRecord class.
    /// </summary>
    [CustomEditor(typeof(ScreenRecord))]
    public class ScreenRecordEditor : Editor
    {
        private SerializedProperty _propStartRecord;
        private SerializedProperty _propStopRecord;
        private SerializedProperty _propLastCapture;
        private SerializedProperty _propFileSize;
        private SerializedProperty _propVideoLength;
        private SerializedProperty _propRecordList;
        private SerializedProperty _propOutputFolderType;
        private SerializedProperty _propOutputFolderPath;
        private SerializedProperty _propDownScale;
        private SerializedProperty _propFrame;
        private SerializedProperty _propPrefix;

        private readonly static GUIContent _guiContentStartRecord = new GUIContent("StartRecord");
        private readonly static GUIContent _guiContentStopRecord = new GUIContent("StopRecord");
        private readonly static GUIContent _guiContentLastCapture = new GUIContent("LastCapture");
        private readonly static GUIContent _guiContentFileSize = new GUIContent("fileSize");
        private readonly static GUIContent _guiContentVideoLength = new GUIContent("VideoLength");
        private readonly static GUIContent _guiContentRecordList = new GUIContent("RecordList");
        private readonly static GUIContent _guiContentOutputFolderType = new GUIContent("OutputFolderType");
        private readonly static GUIContent _guiContentSubfolders = new GUIContent("Subfolder(s)");
        private readonly static GUIContent _guiContentPath = new GUIContent("Path");
        private readonly static GUIContent _guiContentDownScale = new GUIContent("DownScale");
        private readonly static GUIContent _guiContentFrame = new GUIContent("Frame");
        private readonly static GUIContent _guiContentPrefix = new GUIContent("Prefix");

        private ScreenRecord screenRecord { get { return target as ScreenRecord; } }

        /// <summary>
        /// Initializes the properties of the ScreenRecordEditor class.
        /// </summary>
        /// <param name="serializedObject">The serialized object of the ScreenRecordEditor.</param>
        private void OnEnable()
        {
            _propStartRecord = serializedObject.AssertFindProperty("startRecord");
            _propStopRecord = serializedObject.AssertFindProperty("stopRecord");
            _propLastCapture = serializedObject.AssertFindProperty("lastCapture");
            _propFileSize = serializedObject.AssertFindProperty("fileSize");
            _propVideoLength = serializedObject.AssertFindProperty("videoLength");
            _propRecordList = serializedObject.AssertFindProperty("recordList");
            _propOutputFolderType = serializedObject.AssertFindProperty("outputFolderType");
            _propOutputFolderPath = serializedObject.AssertFindProperty("outputFolderPath");
            _propDownScale = serializedObject.AssertFindProperty("downScale");
            _propFrame = serializedObject.AssertFindProperty("frame");
            _propPrefix = serializedObject.AssertFindProperty("prefix");
        }

        /// <summary>
        /// Draws the inspector GUI for a ScreenRecord component.
        /// </summary>
        /// <param name="serializedObject">The serialized object containing the ScreenRecord component.</param>
        /// <returns>void</returns>
        public override void OnInspectorGUI()
        {
            serializedObject.Update();

            EditorGUILayout.PropertyField(_propStartRecord, _guiContentStartRecord);
            EditorGUILayout.PropertyField(_propStopRecord, _guiContentStopRecord);
            EditorGUILayout.PropertyField(_propLastCapture, _guiContentLastCapture);
            EditorGUILayout.PropertyField(_propFileSize, _guiContentFileSize);
            EditorGUILayout.PropertyField(_propVideoLength, _guiContentVideoLength);
            EditorGUILayout.PropertyField(_propRecordList, _guiContentRecordList);
            EditorGUILayout.PropertyField(_propOutputFolderType, _guiContentOutputFolderType);

            if (screenRecord.outputFolderType == OutputPath.Absolute)
            {
                EditorGUILayout.BeginHorizontal();
                EditorGUILayout.PropertyField(_propOutputFolderPath, _guiContentPath);

                if (GUILayout.Button(">", GUILayout.Width(22)))
                {
                    _propOutputFolderPath.stringValue = EditorUtility.SaveFolderPanel("Select Folder To Store Video Captures", System.IO.Path.GetFullPath(System.IO.Path.Combine(Application.dataPath, "../")), "");
                }
                EditorGUILayout.EndHorizontal();
            }
            else
            {
                EditorGUILayout.PropertyField(_propOutputFolderPath, _guiContentSubfolders);
            }

            EditorGUILayout.PropertyField(_propDownScale, _guiContentDownScale);
            EditorGUILayout.PropertyField(_propFrame, _guiContentFrame);
            EditorGUILayout.PropertyField(_propPrefix, _guiContentPrefix);

            if (serializedObject.ApplyModifiedProperties())
            {
                EditorUtility.SetDirty(target);
            }
        }
    }
}

