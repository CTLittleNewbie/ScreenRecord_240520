using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RotateSample : MonoBehaviour
{
    // Update is called once per frame
    void Update()
    {
        //// 创建一个旋转四元数，绕y轴旋转
        //Quaternion rotation = Quaternion.Euler(0, Time.deltaTime * 100, 0);

        //// 应用旋转到cube的Transform组件
        //transform.rotation *= rotation;

        // 计算旋转角度
        float rotationAngle = Time.deltaTime * 100;

        // 创建一个旋转四元数，绕世界y轴旋转
        Quaternion rotation = Quaternion.AngleAxis(rotationAngle, Vector3.up);

        // 应用旋转到cube的Transform组件
        transform.rotation *= rotation;
    }
}
